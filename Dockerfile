FROM atlassian/default-image:2
# Install additional OS packages.
RUN curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash
RUN apt-get -y install git-lfs
# clean up
RUN rm -rf /var/lib/apt/lists/*
